
package com.avenuecode.interfacesWsdlClazz;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.avenuecode.interfacesWsdlClazz package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.avenuecode.interfacesWsdlClazz
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SalaryAverage }
     * 
     */
    public SalaryAverage createSalaryAverage() {
        return new SalaryAverage();
    }

    /**
     * Create an instance of {@link SalaryBonus }
     * 
     */
    public SalaryBonus createSalaryBonus() {
        return new SalaryBonus();
    }

    /**
     * Create an instance of {@link AuthenticationHeader }
     * 
     */
    public AuthenticationHeader createAuthenticationHeader() {
        return new AuthenticationHeader();
    }

    /**
     * Create an instance of {@link APIUsageInformation }
     * 
     */
    public APIUsageInformation createAPIUsageInformation() {
        return new APIUsageInformation();
    }

    /**
     * Create an instance of {@link SalaryAverageResponse }
     * 
     */
    public SalaryAverageResponse createSalaryAverageResponse() {
        return new SalaryAverageResponse();
    }

    /**
     * Create an instance of {@link SalaryBonusResponse }
     * 
     */
    public SalaryBonusResponse createSalaryBonusResponse() {
        return new SalaryBonusResponse();
    }

    /**
     * Create an instance of {@link SalaryAverageFault }
     * 
     */
    public SalaryAverageFault createSalaryAverageFault() {
        return new SalaryAverageFault();
    }

    /**
     * Create an instance of {@link SalaryBonusFault }
     * 
     */
    public SalaryBonusFault createSalaryBonusFault() {
        return new SalaryBonusFault();
    }

}
